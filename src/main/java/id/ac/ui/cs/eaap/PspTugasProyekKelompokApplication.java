package id.ac.ui.cs.eaap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PspTugasProyekKelompokApplication {

	public static void main(String[] args) {
		SpringApplication.run(PspTugasProyekKelompokApplication.class, args);
	}
}
