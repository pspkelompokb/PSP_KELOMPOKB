package id.ac.ui.cs.eaap.kelompokA.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import id.ac.ui.cs.eaap.kelompokA.domain.FakultasModel;
import id.ac.ui.cs.eaap.kelompokA.service.FakultasService;

@RequestMapping("/kelA/fakultas")
@Controller
public class FakultasController {
	@Autowired
	FakultasService fakultasDAO;
	
	@RequestMapping("")
	public String fakultas(Model model)
	{
		List<FakultasModel> fakultass = fakultasDAO.retrieveAllDataModel();
    	model.addAttribute ("fakultass", fakultass);
		return "fakultas/index";
	}
	
	@RequestMapping("/add")
	public String add()
	{
		return "fakultas/add";
	}
	
	@RequestMapping("/addSubmit")
	public void addSubmit(HttpServletResponse response, @ModelAttribute FakultasModel fakultas){
		try {
			fakultasDAO.addDataModel(fakultas);
			response.sendRedirect("/kelA/fakultas");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/edit/{kodeFakultas}")
	public String editPath(Model model, @PathVariable(value = "kodeFakultas") int kodeFakultas)
	{
		FakultasModel fakultas = fakultasDAO.getDataModel(kodeFakultas);
		
		if(fakultas != null)
    	{
        	model.addAttribute ("fakultas", fakultas);
    		return "fakultas/edit";
    	}
    	else
    	{
        	return "fakultas/not-found";
    	}
	}
	
	@RequestMapping("/editSubmit")
	public void editSubmit(HttpServletResponse response, @ModelAttribute FakultasModel fakultas){
		try {
			fakultasDAO.editDataModel(fakultas);
			response.sendRedirect("/kelA/fakultas");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/delete/{kodeFakultas}")
	public void deleteSubmit(HttpServletResponse response, @PathVariable(value = "kodeFakultas") int kodeFakultas)
	{
		try {
			fakultasDAO.deleteDataModel(kodeFakultas);
			response.sendRedirect("/kelA/fakultas");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
