package id.ac.ui.cs.eaap.kelompokA.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import id.ac.ui.cs.eaap.kelompokA.domain.KurikulumModel;
import id.ac.ui.cs.eaap.kelompokA.service.KurikulumService;

@RequestMapping("/kelA/kurikulum")
@Controller
public class KurikulumController {
	@Autowired
	KurikulumService kurikulumDAO;
	
	@RequestMapping("")
	public String kurikulum(Model model)
	{
		List<KurikulumModel> kurikulums = kurikulumDAO.retrieveAllDataModel();
    	model.addAttribute ("kurikulums", kurikulums);
		return "kurikulum/index";
	}
	
	@RequestMapping("/add")
	public String add()
	{
		return "kurikulum/add";
	}
	
	@RequestMapping("/addSubmit")
	public void addSubmit(HttpServletResponse response, @ModelAttribute KurikulumModel kurikulum){
		try {
			kurikulumDAO.addDataModel(kurikulum);
			response.sendRedirect("/kelA/kurikulum");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/edit/{kodeKurikulum}")
	public String editPath(Model model, @PathVariable(value = "kodeKurikulum") int kodeKurikulum)
	{
		KurikulumModel kurikulum = kurikulumDAO.getDataModel(kodeKurikulum);
		
		if(kurikulum != null)
    	{
        	model.addAttribute ("kurikulum", kurikulum);
    		return "kurikulum/edit";
    	}
    	else
    	{
        	return "kurikulum/not-found";
    	}
	}
	
	@RequestMapping("/editSubmit")
	public void editSubmit(HttpServletResponse response, @ModelAttribute KurikulumModel kurikulum){
		try {
			kurikulumDAO.editDataModel(kurikulum);
			response.sendRedirect("/kelA/kurikulum");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/delete/{kodeKurikulum}")
	public void deleteSubmit(HttpServletResponse response, @PathVariable(value = "kodeKurikulum") int kodeKurikulum)
	{
		try {
			kurikulumDAO.deleteDataModel(kodeKurikulum);
			response.sendRedirect("/kelA/kurikulum");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
