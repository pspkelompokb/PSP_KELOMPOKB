package id.ac.ui.cs.eaap.kelompokA.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import id.ac.ui.cs.eaap.kelompokA.domain.UnivModel;
import id.ac.ui.cs.eaap.kelompokA.service.UnivService;

@RequestMapping("/kelA/univ")
@Controller
public class UnivController {
	@Autowired
	UnivService univDAO;
	
	@RequestMapping("")
	public String univ(Model model)
	{
		List<UnivModel> univs = univDAO.retrieveAllDataModel();
    	model.addAttribute ("univs", univs);
		return "univ/index";
	}
	
	@RequestMapping("/add")
	public String add()
	{
		return "univ/add";
	}
	
	@RequestMapping("/addSubmit")
	public void addSubmit(HttpServletResponse response, @ModelAttribute UnivModel univ){
		try {
			univDAO.addDataModel(univ);
			response.sendRedirect("/kelA/univ");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/edit/{kodeUniv}")
	public String editPath(Model model, @PathVariable(value = "kodeUniv") int kodeUniv)
	{
		UnivModel univ = univDAO.getDataModel(kodeUniv);
		
		if(univ != null)
    	{
        	model.addAttribute ("univ", univ);
    		return "univ/edit";
    	}
    	else
    	{
        	return "univ/not-found";
    	}
	}
	
	@RequestMapping("/editSubmit")
	public void editSubmit(HttpServletResponse response, @ModelAttribute UnivModel univ){
		try {
			univDAO.editDataModel(univ);
			response.sendRedirect("/kelA/univ");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping("/delete/{kodeUniv}")
	public void deleteSubmit(HttpServletResponse response, @PathVariable(value = "kodeUniv") int kodeUniv)
	{
		try {
			univDAO.deleteDataModel(kodeUniv);
			response.sendRedirect("/kelA/univ");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
