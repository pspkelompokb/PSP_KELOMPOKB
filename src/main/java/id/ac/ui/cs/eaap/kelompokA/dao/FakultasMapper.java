package id.ac.ui.cs.eaap.kelompokA.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.ac.ui.cs.eaap.kelompokA.domain.FakultasModel;
//import id.ac.ui.cs.eaap.kelompokA.domain.UnivModel;



@Mapper
public interface FakultasMapper {
	
	@Select("select kode_fakultas, nama_fakultas from FAKULTAS "
			+ "where kode_fakultas = #{kodeFakultas}")
	@Results(value = {
		@Result(property="kodeFakultas", column="kode_fakultas"),
		@Result(property="namaFakultas", column="nama_fakultas")
	})
	
	FakultasModel selectFakultas(@Param("kodeFakultas") int kode_fakultas);
	
	@Select("select kode_fakultas, nama_fakultas from FAKULTAS")
	@Results(value = {
		@Result(property="kodeFakultas", column="kode_fakultas"),
		@Result(property="namaFakultas", column="nama_fakultas")
	})
	List<FakultasModel> selectAllFakultas();
	
	@Insert("INSERT INTO FAKULTAS (kode_fakultas, nama_fakultas) "
			+ "VALUES (#{kodeFakultas}, #{namaFakultas})")
	void addFakultas(FakultasModel fakultas);
	
	@Update("UPDATE FAKULTAS set nama_fakultas = #{fakultas.namaFakultas} "
			+ "where kode_fakultas = #{fakultas.kodeFakultas}")
	void updateFakultas(@Param("fakultas") FakultasModel newFakultas);
	
	@Delete("DELETE FROM FAKULTAS where kode_fakultas = #{kodeFakultas}")
	void deleteFakultas(@Param("kodeFakultas") int idFakultas);
	
}
