package id.ac.ui.cs.eaap.kelompokA.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.ac.ui.cs.eaap.kelompokA.domain.KurikulumModel;

@Mapper
public interface KurikulumMapper {
	@Select("select kode_kurikulum, nama_kurikulum, standar_kelulusan from KURIKULUM "
			+ "where kode_kurikulum = #{kodeKurikulum}")
	@Results(value = {
		@Result(property="kodeKurikulum", column="kode_kurikulum"),
		@Result(property="namaKurikulum", column="nama_kurikulum"),
		@Result(property="standarKelulusan", column="standar_kelulusan")
	})
	KurikulumModel selectKurikulum(@Param("kodeKurikulum") int idKurikulum);
	
	@Select("select kode_kurikulum, nama_kurikulum, standar_kelulusan from KURIKULUM")
	@Results(value = {
		@Result(property="kodeKurikulum", column="kode_kurikulum"),
		@Result(property="namaKurikulum", column="nama_kurikulum"),
		@Result(property="standarKelulusan", column="standar_kelulusan")
	})
	List<KurikulumModel> selectAllKurikulum();
	
	@Insert("INSERT INTO KURIKULUM (kode_kurikulum, nama_kurikulum) "
			+ "VALUES (#{kodeKurikulum}, #{namaKurikulum})")
	void addKurikulum(KurikulumModel kurikulum);
	
	@Update("UPDATE KURIKULUM set nama_kurikulum = #{kurikulum.namaKurikulum} "
			+ "where kode_kurikulum = #{kurikulum.kodeKurikulum}")
	void updateKurikulum(@Param("kurikulum") KurikulumModel newKurikulum);
	
	@Delete("DELETE FROM KURIKULUM where kode_kurikulum = #{kodeKurikulum}")
	void deleteKurikulum(@Param("kodeKurikulum") int idKurikulum);
	
}
