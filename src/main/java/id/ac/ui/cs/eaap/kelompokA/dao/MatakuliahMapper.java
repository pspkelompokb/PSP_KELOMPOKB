package id.ac.ui.cs.eaap.kelompokA.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.ac.ui.cs.eaap.kelompokA.domain.MatakuliahModel;

@Mapper
public interface MatakuliahMapper {
	@Select("select kode_mk, nama_mk, sks_mk, kode_fakultas, deskripsi from MATAKULIAH "
			+ "where kode_mk = #{kodeMataKuliah}")
	@Results(value = {
		@Result(property="kodeMataKuliah", column="kode_mk"),
		@Result(property="namaMataKuliah", column="nama_mk"),
		@Result(property="sks", column="sks_mk"),
		@Result(property="kodeFakultas", column="kode_fakultas"),
		@Result(property="deskripsi", column="deskripsi")
	})
	MatakuliahModel selectMatakuliah(@Param("kodeMataKuliah") int kodeMataKuliah);
	
	@Select("select kode_mk, nama_mk, sks_mk, kode_fakultas, deskripsi from MATAKULIAH")
	@Results(value = {
		@Result(property="kodeMataKuliah", column="kode_mk"),
		@Result(property="namaMataKuliah", column="nama_mk"),
		@Result(property="sks", column="sks_mk"),
		@Result(property="kodeFakultas", column="kode_fakultas"),
		@Result(property="deskripsi", column="deskripsi")
	})
	List<MatakuliahModel> selectAllMatakuliah();
	
	@Insert("INSERT INTO MATAKULIAH (kode_mk, nama_mk, sks_mk, kode_fakultas, deskripsi) "
			+ "VALUES (#{kodeMataKuliah}, #{namaMataKuliah}, #{sks}, #{kodeFakultas}, #{deskripsi})")
	void addMatakuliah(MatakuliahModel matakuliah);
	
	@Update("UPDATE MATAKULIAH set nama_mk = #{matakuliah.namaMataKuliah},"
			+ "sks_mk = #{matakuliah.sks},kode_fakultas = #{matakuliah.kodeFakultas}"
			+ "deskripsi = #{matakuliah.deskripsi}"
			+ "where kode_mk = #{matakuliah.kodeMataKuliah}")
	void updateMatakuliah(@Param("matakuliah") MatakuliahModel matakuliah);
	
	@Delete("DELETE FROM MATAKULIAH where kode_mk = #{kodeMataKuliah}")
	void deleteMatakuliah(@Param("kodeMataKuliah") int kodeMataKuliah);
}
