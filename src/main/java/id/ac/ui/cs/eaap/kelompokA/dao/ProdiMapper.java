package id.ac.ui.cs.eaap.kelompokA.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import id.ac.ui.cs.eaap.kelompokA.domain.ProdiModel;

@Mapper
public interface ProdiMapper {
	ProdiModel selectProdi(String kodeProdi);
	
	List<ProdiModel> selectAllProdi();
	
	void addProdi(ProdiModel prodi);
	
	void updateProdi(ProdiModel prodi);
	
	void deleteProdi(String kodeProdi);
}
