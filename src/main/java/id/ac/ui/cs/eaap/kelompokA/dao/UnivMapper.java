package id.ac.ui.cs.eaap.kelompokA.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import id.ac.ui.cs.eaap.kelompokA.domain.UnivModel;

@Mapper
public interface UnivMapper {
	@Select("SELECT * FROM univ WHERE kode_univ = #{kodeUniv}")
	@Results(value = {
			@Result(property="kodeUniv", column="kode_univ"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ")
	})
	UnivModel selectUniv(int kodeUniv);
	
	@Select("SELECT * FROM univ")
	@Results(value = {
			@Result(property="kodeUniv", column="kode_univ"),
			@Result(property="namaUniv", column="nama_univ"),
			@Result(property="urlUniv", column="url_univ")
	})
	List<UnivModel> selectAllUniv();
	
	@Insert("INSERT INTO univ (nama_univ, url_univ) VALUES (#{namaUniv}, #{urlUniv})")
	void addUniv(UnivModel univ);
	
	@Update("UPDATE univ SET nama_univ = #{namaUniv}, url_univ = #{urlUniv} WHERE kode_univ = #{kodeUniv}")
	void updateUniv(UnivModel univ);
	
	@Delete("DELETE FROM univ WHERE kode_univ = #{kodeUniv}")
	void deleteUniv(int kodeUniv);
}
