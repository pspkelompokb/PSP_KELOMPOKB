package id.ac.ui.cs.eaap.kelompokA.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FakultasModel {
	private int kodeFakultas;
	private String namaFakultas;
	private int kodeUniversitas;
	private int kepalaFakultas;
	private List<ProdiModel> listProdi;
}
