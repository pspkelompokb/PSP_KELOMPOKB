package id.ac.ui.cs.eaap.kelompokA.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KurikulumModel {
	private int kodeKurikulum;
	private String namaKurikulum;
	private int standarKelulusan;
}
