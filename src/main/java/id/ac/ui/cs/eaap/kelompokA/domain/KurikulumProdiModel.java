package id.ac.ui.cs.eaap.kelompokA.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KurikulumProdiModel {
	private int kodeKurikulum;
	private int kodeProdi;
	private int kodemk;
	private boolean wajib;
}
