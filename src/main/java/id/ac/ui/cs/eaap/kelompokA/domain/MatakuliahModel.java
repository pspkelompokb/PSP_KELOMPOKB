package id.ac.ui.cs.eaap.kelompokA.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MatakuliahModel {
	private int kodeMataKuliah;
	private String namaMataKuliah;
	private int sks;
	private int kodeFakultas;
	private String deskripsi;
}
