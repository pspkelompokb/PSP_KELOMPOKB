package id.ac.ui.cs.eaap.kelompokA.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProdiModel {
	private int kodeProdi;
	private String namaProdi;
	private int kodeFakultas;
	private int kepalaJabatan;
}
