package id.ac.ui.cs.eaap.kelompokA.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UnivModel {
	private int kodeUniv;
	private String namaUniv;
	private String urlUniv;
	private int kepalaJabatan;
	private List<FakultasModel> listFakultas;
}