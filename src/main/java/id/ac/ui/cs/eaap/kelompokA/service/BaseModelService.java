package id.ac.ui.cs.eaap.kelompokA.service;

import java.util.List;

public interface BaseModelService<T> {
	T getDataModel(int idPrimaryKey);
	
	List<T> retrieveAllDataModel();
	
	void addDataModel(T newDataModel);
	
	void editDataModel(T updatedDataModel);
	
	void deleteDataModel(int idPrimaryKey);
}
