package id.ac.ui.cs.eaap.kelompokA.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.ui.cs.eaap.kelompokA.dao.FakultasMapper;
import id.ac.ui.cs.eaap.kelompokA.domain.FakultasModel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FakultasServiceDatabase implements FakultasService{
	@Autowired
	private FakultasMapper kurikulumMapper;

	@Override
	public FakultasModel getDataModel(int idFakultas) {
		log.info ("retrieve Fakultas with id {}", idFakultas);
		return kurikulumMapper.selectFakultas(idFakultas);
	}

	@Override
	public List<FakultasModel> retrieveAllDataModel() {
		log.info ("get all Fakultas");
		return kurikulumMapper.selectAllFakultas();
	}

	@Override
	public void addDataModel(FakultasModel newFakultas) {
		log.info ("add new Fakultas: {}, {}, {}", newFakultas.getKodeFakultas(), newFakultas.getNamaFakultas());
		kurikulumMapper.addFakultas(newFakultas);
	}

	@Override
	public void editDataModel(FakultasModel updatedFakultas) {
		log.info ("update Fakultas with id {}, to: {}", updatedFakultas.getKodeFakultas(), updatedFakultas.getNamaFakultas());
		kurikulumMapper.updateFakultas(updatedFakultas);
	}

	@Override
	public void deleteDataModel(int idFakultas) {
		log.info ("delete Fakultas with id: {}", idFakultas);
		kurikulumMapper.deleteFakultas(idFakultas);
	}

}
