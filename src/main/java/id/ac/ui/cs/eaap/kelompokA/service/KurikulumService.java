package id.ac.ui.cs.eaap.kelompokA.service;

import id.ac.ui.cs.eaap.kelompokA.domain.KurikulumModel;

public interface KurikulumService extends BaseModelService<KurikulumModel>{
	//this left empty intentionally
}
