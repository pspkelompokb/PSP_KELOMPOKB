package id.ac.ui.cs.eaap.kelompokA.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.ui.cs.eaap.kelompokA.dao.KurikulumMapper;
import id.ac.ui.cs.eaap.kelompokA.domain.KurikulumModel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class KurikulumServiceDatabase implements KurikulumService{
	@Autowired
	private KurikulumMapper kurikulumMapper;

	@Override
	public KurikulumModel getDataModel(int idKurikulum) {
		log.info ("retrieve Kurikulum with id {}", idKurikulum);
		return kurikulumMapper.selectKurikulum(idKurikulum);
	}

	@Override
	public List<KurikulumModel> retrieveAllDataModel() {
		log.info ("get all Kurikulum");
		return kurikulumMapper.selectAllKurikulum();
	}

	@Override
	public void addDataModel(KurikulumModel newKurikulum) {
		log.info ("add new Kurikulum: {}, {}, {}", newKurikulum.getKodeKurikulum(), newKurikulum.getNamaKurikulum());
		kurikulumMapper.addKurikulum(newKurikulum);
	}

	@Override
	public void editDataModel(KurikulumModel updatedKurikulum) {
		log.info ("update Kurikulum with id {}, to: {}", updatedKurikulum.getKodeKurikulum(), updatedKurikulum.getNamaKurikulum());
		kurikulumMapper.updateKurikulum(updatedKurikulum);
	}

	@Override
	public void deleteDataModel(int idKurikulum) {
		log.info ("delete Kurikulum with id: {}", idKurikulum);
		kurikulumMapper.deleteKurikulum(idKurikulum);
	}

}
