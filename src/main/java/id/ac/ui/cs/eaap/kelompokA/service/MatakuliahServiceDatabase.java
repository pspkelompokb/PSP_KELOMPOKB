package id.ac.ui.cs.eaap.kelompokA.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.ui.cs.eaap.kelompokA.dao.MatakuliahMapper;
import id.ac.ui.cs.eaap.kelompokA.domain.MatakuliahModel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MatakuliahServiceDatabase implements MatakuliahService {
	@Autowired
	private MatakuliahMapper matakuliahMapper;

	@Override
	public MatakuliahModel getDataModel(int idMk) {
		log.info ("retrieve Matakuliah with id {}", idMk);
		return matakuliahMapper.selectMatakuliah(idMk);
	}

	@Override
	public List<MatakuliahModel> retrieveAllDataModel() {
		log.info ("get all Matakuliah");
		return matakuliahMapper.selectAllMatakuliah();
	}

	@Override
	public void addDataModel(MatakuliahModel newMK) {
		log.info ("add new Matakuliah: {}, {}, {}, {}, {}", newMK.getKodeMataKuliah(), 
				newMK.getNamaMataKuliah(), newMK.getKodeFakultas(), newMK.getSks(), newMK.getDeskripsi());
		matakuliahMapper.addMatakuliah(newMK);
	}

	@Override
	public void editDataModel(MatakuliahModel updatedMK) {
		log.info ("update Matakuliah with id {}, to: {}, {}, {}, {}", updatedMK.getKodeMataKuliah(), 
				updatedMK.getNamaMataKuliah(), updatedMK.getKodeFakultas(), updatedMK.getSks(), updatedMK.getDeskripsi());
		matakuliahMapper.updateMatakuliah(updatedMK);

	}

	@Override
	public void deleteDataModel(int idMk) {
		log.info ("delete Matakuliah with id: {}", idMk);
		matakuliahMapper.deleteMatakuliah(idMk);
	}

}
