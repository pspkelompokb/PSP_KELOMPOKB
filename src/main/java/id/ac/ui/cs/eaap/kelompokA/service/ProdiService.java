package id.ac.ui.cs.eaap.kelompokA.service;

import java.util.List;

import id.ac.ui.cs.eaap.kelompokA.domain.ProdiModel;

public interface ProdiService {
	ProdiModel selectProdi(String kodeProdi);
	
	List<ProdiModel> selectAllProdi();
	
	void addProdi(ProdiModel prodi);
	
	void updateProdi(ProdiModel prodi);
	
	void deleteProdi(String kodeProdi);
}
