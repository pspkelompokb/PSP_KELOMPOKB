package id.ac.ui.cs.eaap.kelompokA.service;

import id.ac.ui.cs.eaap.kelompokA.domain.UnivModel;

public interface UnivService extends BaseModelService<UnivModel>
{
	//this leaves empty
}
