package id.ac.ui.cs.eaap.kelompokA.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.ac.ui.cs.eaap.kelompokA.dao.UnivMapper;
import id.ac.ui.cs.eaap.kelompokA.domain.UnivModel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UnivServiceDatabase implements UnivService{
	@Autowired
	private UnivMapper univMapper;

	@Override
	public UnivModel getDataModel(int idPrimaryKey) {
		log.info("select univ with kode {}", idPrimaryKey);
		return univMapper.selectUniv(idPrimaryKey);
	}

	@Override
	public List<UnivModel> retrieveAllDataModel() {
		log.info("select all univs");
		return univMapper.selectAllUniv();
	}

	@Override
	public void addDataModel(UnivModel newDataModel) {
		log.info("add univ");
		univMapper.addUniv(newDataModel);
	}

	@Override
	public void editDataModel(UnivModel updatedDataModel) {
		log.info("update univ with kode {}", updatedDataModel.getKodeUniv());
		univMapper.updateUniv(updatedDataModel);
	}

	@Override
	public void deleteDataModel(int idPrimaryKey) {
		log.info("delete unit with kode {}", idPrimaryKey);
		univMapper.deleteUniv(idPrimaryKey);
	}

}
